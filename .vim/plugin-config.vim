" HTML, JSX
" =============================================================================
" https://github.com/alvan/vim-closetag
let g:closetag_filenames = '*.html,*.js,*.jsx,*.ts,*.tsx'

"  Nerdtree
let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1
let NERDTreeAutoDeleteBuffer=1
let NERDTreeMinimalUI=1
let NERDTreeDirArrows=1
let NERDTreeShowLineNumbers=1
let NERDTreeMapOpenInTab='\t'

" material.vim
" =============================================================================
" https://github.com/kaicataldo/material.vim

let g:material_terminal_italics = 1
let g:material_theme_style = 'ocean'
colorscheme material

if (has("termguicolors"))
    set termguicolors
endif

let g:lightline = { 'colorscheme': 'material_vim' }
