let mapleader=" "

" shorter commands
cnoreabbrev tree NERDTreeToggle
cnoreabbrev find NERDTreeFind

" plugs
map <Leader>nt :NERDTreeFind<CR>

